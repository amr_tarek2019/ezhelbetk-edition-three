<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>invoice</title>
    <link rel="stylesheet" href="{{asset('assets/invoice/style.css')}}" media="all" />
</head>
<body onload="window.print();">
<header class="clearfix">
    <div id="logo">
        <img src="{{ asset($settings->image) }}" alt="ambeco">
    </div>
    <h1>Ezhalbaitk</h1>

    <div id="project">
        <div><span>{{trans('reservation.invoice number')}}</span>{{$data->order_number}}</div>
        <div><span>{{trans('user.name')}}</span> <span >{{$data->user->name}}</span></div>
        <div><span>{{trans('reservation.createdat')}}</span> {{$data->created_at}}</div>
        <div><span>{{trans('reservation.client number')}}</span> {{$data->user->id}}</div>
        <div><span>{{trans('user.phone')}}</span>{{$data->user->phone}}</div>
        <div><span>{{trans('user.email')}}</span>{{$data->user->email}}</div>
        <div><span>{{trans('reservation.address')}}</span>{{$data->user->address}}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th>{{trans('reservation.Item No')}}</th>
            <th>{{trans('reservation.Statement')}}</th>
            <th class="service">{{trans('reservation.quantity')}}</th>
            <th class="desc">{{trans('reservation.Unit price / riyal')}}</th>

        </tr>
        </thead>
        <tbody>

        <tr>
            <td class="service">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->id}}<br>
                @endforeach
            </td>
            <td class="desc">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->details_en}}<br>
                @endforeach
            </td>
            <td class="qty">
              @foreach($orderSubcategories as $orderSubcategory)
                    <br>{{$orderSubcategory->quantity}}<br>
              @endforeach
            </td>
            <td class="total">
                @foreach($subcategories as $subcategory)
                    <br>{{$subcategory->price}} {{$subcategory->currency}}<br>
                @endforeach
            </td>

        </tr>



        <tr>
            <td class="grand total">
             {{\App\OrderSubcategory::where('order_id',$data->id)->sum('total')}}
            </td>
<td colspan="7" class="grand total">{{trans('reservation.total')}} </td>



</tr>



</tbody>
</table>

<div class="signature">
<div id="project">
<div><span>توقيع المسئول </span> ...................................................</div>
<div><span>البائع </span> ...................................................</div>
<div><span>التوقيع  </span>...................................................</div>

</div>
<div id="project2">
<div><span> المستلم </span> ...................................................</div>
<div><span> التوقيع</span> ...................................................</div>


</div>
</div>
</main>


</body>
</html>
