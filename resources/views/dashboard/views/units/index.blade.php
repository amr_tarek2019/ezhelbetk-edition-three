@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('unit.units DataTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('unit.units')}}</li>
                                <li class="breadcrumb-item active">{{trans('unit.units DataTable')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('units.create') }}" class="btn btn-primary">{{trans('city.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('unit.UNITS DETAILS')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('city.nameenglish')}}</th>
                                        <th>{{trans('city.namearabic')}}</th>
                                        <th>{{trans('category.image')}}</th>
                                        <th>{{trans('category.status')}}</th>
                                        <th width="10%">{{trans('category.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($units as $key => $unit)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$unit->name_en}}</td>
                                            <td>{{$unit->name_ar}}</td>
                                            <td>
                                                <img class="img-responsive img-thumbnail" src="{{ asset($unit->image) }}" style="height: 100px; width: 100px" alt="">
                                            </td>
                                            <td>
                                                <div class="media-body text-left switch-lg icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateSubcategoryStatus(this)" value="{{ $unit->id }}" type="checkbox"
                                                        <?php if($unit->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>

                                            <td>
                                                <a href="{{ route('units.edit',$unit->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('category.edit')}}</i></a>

                                                <form id="delete-form-{{ $unit->id }}" action="{{ route('units.destroy',$unit->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('{{trans('technician.Areyousure?Youwanttodeletethis?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $unit->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">{{trans('category.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('myjsfile')
    <script>
    function updateSubcategoryStatus(ell){
    if(ell.checked){
    var status = 1;
    }
    else{
    var status = 0;
    }
    $.post('{{ route('units.status',isset($unit) ? $unit->id : "" ) }}', {_token:'{{ csrf_token() }}', id:ell.value, status:status}, function(data){
    if(data == 1){
    alert('unit status changed successfully');
    }
    else{
    alert('{{trans('subcategory.danger,Somethingwentwrong')}}');
    }
    });
    }

    </script>
    @endsection
