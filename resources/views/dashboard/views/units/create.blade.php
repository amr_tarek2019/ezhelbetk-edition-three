@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('unit.Add unit')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('unit.units')}}</li>
                            <li class="breadcrumb-item active">{{trans('unit.Add unit')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('category.completeform')}}</h5>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" action="{{ route('units.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('category.titleenglish')}}</label>
                                    <input class="form-control" id="name_en" name="name_en" placeholder="Enter About your description"/>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label class="form-label">{{trans('category.titlearabic')}}</label>
                                    <input class="form-control" id="name_ar" name="name_ar"  placeholder="Enter About your description"/>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="col-md-12">
                                <div class="col-form-label">{{trans('technician.category')}}</div>
                                <select name="category" class="form-control digits">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name_en }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>

<br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('unit.warranty period')}}</label>
                                        <select class="form-control digits" name="warranty_period" id="warranty_period">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="1">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('unit.period in english')}}</label>
                                        <select class="form-control digits" name="period_en" id="period_en">
                                            <option value="year">year</option>
                                            <option value="month">month</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('unit.period in arabic')}}</label>
                                        <select class="form-control digits" name="period_ar" id="period_ar">
                                            <option value="سنة">سنة</option>
                                            <option value="شهر">شهر</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('unit.warranty text en')}}</label>
                                        <textarea class="form-control" id="warranty_text_en" name="warranty_text_en" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('unit.warranty text ar')}}</label>
                                        <textarea class="form-control" id="warranty_text_ar" name="warranty_text_ar" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="form-label" style="margin-left: 30px;">{{trans('category.uploadfile')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="image" name="image" type="file" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                         <button class="btn btn-primary" type="submit">{{trans('category.submit')}}</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection
