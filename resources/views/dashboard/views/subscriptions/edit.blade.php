@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('subscription.edit subscription')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('subscription.subscriptions')}}</li>
                                <li class="breadcrumb-item active">{{trans('subscription.edit subscription')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('subcategory.CompleteForm')}}</h5>
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" novalidate="" action="{{ route('subscriptions.update',$subscription->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('subscription.months')}}</label>
                                        <input class="form-control" type="number" value="{{$subscription->months}}" id="months" name="months" placeholder="months"/>
                                    </div>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <div class="form-group mb-0">
                                        <label class="form-label">{{trans('subscription.price')}}</label>
                                        <input class="form-control" value="{{$subscription->price}}" id="price" name="price"  placeholder="price"/>
                                    </div>
                                </div>
                                <br>

                                <div class="col-md-12">
                                    <div class="col-form-label">{{trans('technician.category')}}</div>
                                    <select name="category" class="form-control digits">
                                        @foreach($categories as $category)
                                            <option {{ $category->id == $subscription->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <br>
                                <div class="col-md-12">
                                    <div class="col-form-label">{{trans('subscription.package')}}</div>
                                    <select name="package" class="form-control digits">
                                        @foreach($packages as $package)
                                            <option {{ $package->id == $subscription->package->id ? 'selected' : '' }} value="{{ $package->id }}">{{ $package->name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <button class="btn btn-primary" type="submit">{{trans('technician.submitform')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
