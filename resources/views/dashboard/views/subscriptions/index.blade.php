@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('subscription.SUBSCRIPTIONS DATATABLE')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('subscription.subscriptions')}}</li>
                                <li class="breadcrumb-item active">{{trans('subscription.SUBSCRIPTIONS DATATABLE')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('subscriptions.create') }}" class="btn btn-primary">{{trans('admin.addnew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('subscription.SUBSCRIPTIONS DETAILS')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('technician.category')}}</th>
                                        <th>{{trans('subscription.package')}}</th>
                                        <th>{{trans('subscription.months')}}</th>
                                        <th>{{trans('subscription.price')}}</th>
                                        <th>{{trans('admin.status')}}</th>
                                        <th width="10%">{{trans('admin.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscriptions as $key => $subscription)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$subscription->category->name_en}}</td>
                                            <td>{{$subscription->package->name_en}}</td>
                                            <td>{{$subscription->months}}</td>
                                            <td>{{$subscription->price}}</td>
                                            <td>
                                                <div class="media-body text-left switch-lg icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateStatus(this)" value="{{ $subscription->id }}" type="checkbox"
                                                        <?php if($subscription->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ route('subscriptions.edit',$subscription->id) }}" class="btn btn-info btn-sm"><i class="material-icons">{{trans('reservation.edit')}}</i></a>

                                                <form id="delete-form-{{ $subscription->id }}" action="{{ route('subscriptions.destroy',$subscription->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('are you sure ? you want to delete this field ?')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $subscription->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }"><i class="material-icons">{{trans('reservation.delete')}}</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('myjsfile')
    <script>
        function updateStatus(ell){
            if(ell.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('subscriptions.status',isset($subscription) ? $subscription->id : "") }}', {_token:'{{ csrf_token() }}', id:ell.value, status:status}, function(data){
                if(data == 1){
                    alert('subscription status changed successfully');
                }
                else{
                    alert('something went wrong');
                }
            });
        }

    </script>
@endsection
