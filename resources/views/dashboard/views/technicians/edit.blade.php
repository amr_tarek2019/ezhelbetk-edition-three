@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('technician.UpdateTechnician')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('technician.Technicians')}}</li>
                                <li class="breadcrumb-item active">{{trans('technician.UpdateTechnician')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        <form class="card" method="POST" action="{{ route('technicians.update',$user->id) }}">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('technician.UpdateTechnician')}}</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('technician.name')}}</label>
                                            <input class="form-control" value="{{$user->name}}" name="name" id="name" type="text" placeholder="{{trans('technician.name')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('technician.email')}}</label>
                                            <input class="form-control" value="{{$user->email}}" name="email" id="email" type="text" placeholder="{{trans('technician.email')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('technician.phone')}}</label>
                                            <input class="form-control" value="{{$user->phone}}" name="phone" id="phone" type="text" placeholder="{{trans('technician.phone')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('technician.address')}}</label>
                                            <input class="form-control"  value="{{$user->address}}" name="address" id="address" type="text" placeholder="address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-form-label">{{trans('technician.category')}}</div>
                                        <select name="category_id" id="category_id" class="form-control digits">
                                            @foreach($categories as $category)
                                                <option {{ $category->id == $technician->category->id ? 'selected' : '' }} id="{{ $category->id }}" value="{{ $category->id }}">{{ $category->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
<br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-form-label">{{trans('technician.unit')}}</div>
                                        <select name="unit_id" id="unit_id" class="form-control digits" >
                                            @foreach($units as $unit)
                                                <option {{ $unit->id == $technician->unit->id ? 'selected' : '' }} value="{{ $unit->id }}">{{ $unit->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                    <br>
                                <div class="row">
                                <div class="col-md-12">
                                        <div class="col-form-label">{{trans('technician.subcategory')}}</div>
                                        <select name="subcategory_id" id="subcategory_id"  class="form-control digits" >
                                            @foreach($subcategories as $subcategory)
                                                <option {{ $subcategory->id == $technician->subcategory->id ? 'selected' : '' }} value="{{ $subcategory->id }}">{{ $subcategory->name_en }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                   <br>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="exampleFormControlSelect9">{{trans('technician.status')}}</label>
                                            <select class="form-control digits" name="is_busy" id="is_busy">
{{--                                                <option value="0">busy</option>--}}
{{--                                                <option value="1">available</option>--}}
                                                <option value="0" {{ isset($technician) && $technician->is_busy == 0 ? 'selected'  :'' }}>available</option>
                                                <option value="1" {{ isset($technician) && $technician->is_busy == 1 ? 'selected'  :'' }}>busy</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('technician.Password')}}</label>
                                            <input class="form-control" type="text" placeholder="{{trans('technician.Password')}}" name="password" id="password">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">{{trans('technician.submitform')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection

@section('myjsfile')

    <script>


        $(document).on('change', '#category_id', function(){
            var category_id = $(this).val();
            //alert(company_id);
            if(category_id){
                $.ajax({
                    type:"GET",
                    // url:"{{url('get-category-units/')}}/?category_id="+category_id,
                    url:"/en/get-category-units/"+category_id,
                    success:function(res){
                        console.log(res);

                        if(res){
                            console.log(res);
                            $("#unit_id").empty();
                            $.each(res,function(key,value){
                                $("#unit_id").append('<option value="'+value+'" >'+key+'</option>');

                            });
                        }
                        if(res.length === 0){
                            $("#unit_id").empty();
                        }
                    }
                });
            }else{
                $("#unit_id").empty();
            }
        });

    </script>


    <script>


        $('#unit_id').on('change', function(){
            var unit_id = $('#unit_id').val();
            // alert(unit_id);
            if(unit_id){
                $.ajax({
                    type:"GET",
                    // url:"{{url('get-subcategory-units/')}}/?category_id="+category_id,
                    url:"/en/get-subcategory-units/"+unit_id,
                    success:function(res){
                        console.log(res);

                        if(res){
                            console.log(res);
                            $("#subcategory_id").empty();
                            $.each(res,function(key,value){
                                $("#subcategory_id").append('<option id="subcategory_id" value="'+value+'" >'+key+'</option>');
                            });
                        }
                        if(res.length === 0){
                            $("#subcategory_id").empty();
                        }
                    }
                });
            }else{
                $("#subcategory_id").empty();
            }
        });

    </script>


@endsection
