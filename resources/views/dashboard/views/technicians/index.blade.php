@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('technician.TechniciansDataTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('technician.Technicians')}}</li>
                                <li class="breadcrumb-item active">{{trans('technician.TechniciansDataTable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('technicians.create') }}" class="btn btn-primary">{{trans('technician.AddNew')}}</a>
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('technician.TechniciansData')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('dashboard.username')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('user.phone')}}</th>
                                        <th>{{trans('admin.status')}}</th>
                                        <th>{{trans('technician.Statusselect')}}</th>
                                        <th>{{trans('category.name')}}</th>
                                        <th>{{trans('user.created')}}</th>
                                        <th>{{trans('user.updated')}}</th>
                                        <th> {{trans('user.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                <label class="switch">
                                                    <input onchange="updateTechnicianStatus(this)" value="{{ $user->id }}" type="checkbox"
                                                    <?php if($user->user_status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                                </div>
                                            </td>
                                            <td>

                                                <div class="media-body text-right icon-state">
                                                    <label class="switch">
                                                        @php
                                                            $technicianStatus=\App\Technician::where('user_id',$user->id)->first();
                                                        @endphp
                                                        <input onchange="updateTechnicianBusy(this)" value="{{ $technicianStatus->id }}" type="checkbox"
                                                        <?php
                                                        if($technicianStatus->is_busy == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                @php
                                                $technicianCategory=\App\Technician::where('user_id',$user->id)->select('category_id')->first();
                                                @endphp
                                                {{$technicianCategory->category->name_en}}
                                            </td>
                                            <td>{{$user->created_at}}</td>
                                            <td>{{$user->updated_at}}</td>
                                            <td>

                                                <a href="{{ route('technicians.show',$user->id) }}" class="btn btn-success">{{trans('reservation.details')}}</a>


                                                <a href="{{ route('technicians.edit',$user->id) }}" class="btn btn-info">{{trans('admin.edit')}}</a>

                                                <form id="delete-form-{{ $user->id }}" action="{{ route('technicians.destroy',$user->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('user.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        function updateTechnicianStatus(elUser){
            if(elUser.checked){
                var user_status = 1;
            }
            else{
                var user_status = 0;
            }
            $.post('{{ route('technicians.status',isset($user) ? $user->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, user_status:user_status}, function(data){
                if(data == 1){
                    alert('{{trans('admin.successchangestatus')}}');
                }
                else{
                    alert('{{trans('admin.successerrorchangestatus')}}');
                }
            });
        }
    </script>

    <script>
        function updateTechnicianBusy(technician){
            if(technician.checked){
                var is_busy = 1;
            }
            else{
                var is_busy = 0;
            }
            $.post('{{ route('technicians.availability',isset($technicianStatus) ? $technicianStatus->id : "") }}', {_token:'{{ csrf_token() }}', id:technician.value, is_busy:is_busy}, function(data){
                if(data == 1){
                    alert('{{trans('admin.successchangestatus')}}');
                }
                else{
                    alert('{{trans('admin.successerrorchangestatus')}}');
                }
            });
        }
    </script>
@endsection
