@extends('dashboard.layouts.master')

@section('content')

{{--<div class="col-lg-6 col-lg-offset-3">--}}
{{--    <div class="panel">--}}
{{--        <div class="panel-heading">--}}
{{--            <h3 class="panel-title">{{__('Role Information')}}</h3>--}}
{{--        </div>--}}

{{--        <!--Horizontal Form-->--}}
{{--        <!--===================================================-->--}}
{{--        <form class="form-horizontal" action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">--}}
{{--        	@csrf--}}
{{--            <div class="panel-body">--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="name">{{__('Name')}}</label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <input type="text" placeholder="{{__('Name')}}" id="name" name="name" class="form-control" required>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="panel-heading">--}}
{{--                    <h3 class="panel-title">{{ __('Permissions') }}</h3>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label class="col-sm-3 control-label" for="banner"></label>--}}
{{--                    <div class="col-sm-9">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Products') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="1">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Flash Deal') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="2">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Orders') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="3">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Sales') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="4">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Sellers') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="5">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Customers') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="6">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Messaging') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="7">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Business Settings') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="8">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Frontend Settings') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="9">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-10">--}}
{{--                                <label class="control-label">{{ __('Staffs') }}</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-2">--}}
{{--                                <label class="switch">--}}
{{--                                    <input type="checkbox" name="permissions[]" class="form-control demo-sw" value="10">--}}
{{--                                    <span class="slider round"></span>--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="panel-footer text-right">--}}
{{--                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--        <!--===================================================-->--}}
{{--        <!--End Horizontal Form-->--}}

{{--    </div>--}}
{{--</div>--}}




<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('permissions.Role Information')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">{{trans('permissions.roles')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('permissions.ADD ROLE')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <form class="form-horizontal" action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                            <div class="col">

                                <div class="form-group">
                                    <label for="name">{{trans('admin.name')}}</label>
                                    <input type="text" placeholder="{{trans('admin.name')}}" id="name" name="name" class="form-control" required>
                                </div>


                                <div class="form-group">


                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-1" value="1" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-1">{{trans('dashboard.users')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-2" value="2" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-2">{{trans('dashboard.admins')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-3" value="3" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-3">{{trans('dashboard.members')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-4" value="4" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-4">{{trans('dashboard.technicians')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-5" value="5" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-5">{{trans('dashboard.cities')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-6" value="6" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-6">{{trans('dashboard.categories')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-7" value="7" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-7">{{trans('dashboard.units')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-8" value="8" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-8">{{trans('dashboard.subcategories')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-9" value="9" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-9">{{trans('dashboard.packages')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-10" value="10" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-10">{{trans('dashboard.subscriptions')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-11" value="11" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-11">{{trans('dashboard.Users subscriptions')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-12" value="12" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-12">{{trans('dashboard.ratings')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-13" value="13" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-13">{{trans('dashboard.orders')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-14" value="14" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-14">{{trans('permissions.permissions')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-15" value="15" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-15">{{trans('dashboard.suggestions')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-16" value="16" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-16">{{trans('dashboard.settings')}}</label>
                                    </div>

                                    <div class="checkbox checkbox-dark m-squar">
                                        <input id="inline-sqr-17" value="17" name="permissions[]" type="checkbox">
                                        <label class="mt-0" for="inline-sqr-17">{{trans('dashboard.notifications')}}</label>
                                    </div>

                                </div>
                                <button class="btn btn-primary" type="submit" data-original-title="" title="">{{trans('city.submitForm')}}</button>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection
