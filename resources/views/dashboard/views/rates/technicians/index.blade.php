@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('rates.TechnicianRatesDataTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('rates.Rates')}}</li>
                                <li class="breadcrumb-item">{{trans('rates.Technicians')}}</li>
                                <li class="breadcrumb-item active">{{trans('rates.TechnicianRatesDataTable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.layouts.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('rates.TechnicianRatesDataTable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('rates.ordernumber')}}</th>
                                        <th>{{trans('rates.rate')}}</th>
                                        <th>{{trans('rates.CreatedAt')}}</th>
                                        <th>{{trans('rates.Actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($techniciansRates as $key=>$technicianRate)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$technicianRate->order->order_number}}</td>
                                            <td>{{$technicianRate->rate}}</td>
                                            <td>{{$technicianRate->created_at}}</td>
                                            <td>
                                                <a href="{{ route('technicians.rates.show',$technicianRate->id) }}" class="btn btn-info">{{trans('rates.Details')}}</a>

                                                <form id="delete-form-{{ $technicianRate->id }}" action="{{ route('users.rates.destroy',$technicianRate->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('rates.Areyousure?Youwanttodeletethis?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $technicianRate->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('rates.Delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection
