<?php
return [
    'phone'=>'phone',
    'email'=>'Email address',
    'logo'=>'logo',
    'aboutE'=>'about in english',
    'aboutA'=>'about in arabic',
    'appname'=>'app name',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'settings'=>'settings',
    'facebook'=>'facebook',
    'google'=>'google',
];
