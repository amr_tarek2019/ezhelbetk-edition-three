<?php
return[
    'USERS SUBSCRIPTIONS TABLE'=>'USERS SUBSCRIPTIONS TABLE',
    'USERS TABLE'=>'USERS TABLE',
    'user name'=>'user name',
    'email'=>'email',
    'subscription'=>'subscription',
    'price'=>'price',
    'status'=>'status',
    'no technician assigned yet'=>'no technician assigned yet',
    'CATEGORY AND PACKAGE DATA'=>'CATEGORY AND PACKAGE DATA',
    'TECHNICIAN DATA'=>'TECHNICIAN DATA',
    'USER DATA'=>'USER DATA',
    'USER SUBSCRIPTION DETAILS'=>'USER SUBSCRIPTION DETAILS',
    'CATEGORY AND PACKAGE DATA'=>'CATEGORY AND PACKAGE DATA',
    'category'=>'category',
    'package'=>'package'
];
