<?php
return[
    'ROLES DATA'=>'بيانات الادوار',
    'Roles DataTable'=>'جدول الادوار',
    'Role Information'=>'معلومات الادوار',
    'roles'=>'الادوار',
    'ADD ROLE'=>'اضافة دور',
    'EDIT ROLE'=>'تعديل دور',
    'permissions'=>'الصلاحيات'
];
