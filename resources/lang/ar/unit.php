<?php
return[
'units DataTable'=>' جدول بيانات الوحدات',
'units'=>'وحدات',
'unit status changed successfully'=>'تم تغيير حالة الوحدة بنجاح',
'Add unit'=>'إضافة وحدة',
'warranty period'=>'فترة الضمان',
'period'=>'فترة',
'warranty text en'=>'نص الضمان بالانجليزية',
'warranty text ar'=>'نص الضمان بالعربية',
   'UNITS DETAILS' =>'تفاصيل الوحدات',
    'period in english'=>'المدة بالانجليزية',
    'period in arabic'=>'المدة بالعربية',
    'Edit unit'=>'تعديل الوحدة'
];
