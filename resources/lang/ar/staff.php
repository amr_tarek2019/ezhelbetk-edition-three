<?php
return[
    'Add Staff'=>'اضافة عضو جديد',
    'staffs'=>'جميع الاعضاء',
    'Role'=>'الوظيفة',
    'Staff DataTables'=>'جدول الاعضاء',
    'Staff Data'=>'بيانات الاعضاء',
    'Edit Staff'=>'تعديل الاعضاء'
];
