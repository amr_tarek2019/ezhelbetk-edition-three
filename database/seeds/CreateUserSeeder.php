<?php

use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(DB::table('users')->get()->count() == 0){

            DB::table('users')->insert([

                [
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123456'),
            'phone'=>'01018472418',
            'address'=>'helwan',
            'lat'=>'0',
            'lng'=>'0',
            'social_type'=>'0',
            'social_token'=>'0',
            'jwt_token'=>'0',
            'firebase_token'=>'0',
            'user_status'=>'1',
            'status'=>'1',
            'verify_code'=>'null',
            'user_type'=>'admin'
                ],
                [
            'name' => 'mahmoud',
            'email' => 'mahmoud@mail.com',
            'password' => bcrypt('123456789'),
            'phone'=>'01234567891',
            'address'=>'kobry el2oba',
            'lat'=>'31.212',
            'lng'=>'32.456',
            'social_type'=>'0',
            'social_token'=>'zxcvbnm123456',
            'jwt_token'=>'zxcvbnm123456',
            'firebase_token'=>'zxcvbnm123456',
            'user_status'=>'1',
            'status'=>'1',
            'verify_code'=>'null',
            'user_type'=>'user'
                ],
                [
                    'name' => 'atef',
                    'email' => 'atef@mail.com',
                    'password' => bcrypt('12345678910'),
                    'phone'=>'012345678910',
                    'address'=>'gamal abd elnaser',
                    'lat'=>'31.2123',
                    'lng'=>'32.4567',
                    'social_type'=>'1',
                    'social_token'=>'zxcvbnm123456',
                    'jwt_token'=>'zxcvbnm123456',
                    'firebase_token'=>'zxcvbnm123456',
                    'user_status'=>'1',
                    'status'=>'1',
                    'verify_code'=>'null',
                    'user_type'=>'technician'
                ],
            ]);

        }
    }
}
