<?php

use Illuminate\Database\Seeder;

class CreatePackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('packages')->get()->count() == 0){

            DB::table('packages')->insert([

                [
                    'name_en' => 'platinum',
                    'name_ar' => 'بلاتين',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'gold',
                    'name_ar' => 'ذهب',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name_en' => 'silver',
                    'name_ar' => 'فضة',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);

        }
    }
}
