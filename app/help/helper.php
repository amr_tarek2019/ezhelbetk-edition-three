<?php

function unreadMsg()
{
    return \App\Contact::where('view',0)->get();
}

function countUnreadMsg()
{
    return \App\Contact::where('view',0)->count();
}


function unActiveOrder()
{
    return \App\Order::where('accepted',0)->get();
}

function countUnActiveOrder()
{
    return \App\Order::where('accepted',0)->count();
}
