<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table='contacts';
    protected $fillable=['user_id', 'username', 'phone', 'problem', 'view'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
