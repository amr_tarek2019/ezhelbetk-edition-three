<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $table='users_subscriptions';
    protected $fillable=['user_id', 'subscription_id', 'total_price',
        'date', 'time', 'subscription_number', 'status', 'accepted','currency'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function subscription()
    {
        return $this->belongsTo('App\Subscription','subscription_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    function technician($id) {
      $data =   UserTechnicianSubscription::where('user_subscription_id',$id)->pluck('technician_id')->first();
      $technician_id = Technician::where('id',$data)->pluck('user_id')->first();
      return User::where('id',$technician_id)->first();
    }

    public function userTechnicianSubscription()
    {
        return $this->belongsTo('App\UserTechnicianSubscription','user_subscription_id');
    }
}
