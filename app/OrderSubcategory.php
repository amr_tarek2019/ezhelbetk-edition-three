<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderSubcategory extends Model
{
    protected $table='order_subcategory';
    protected $fillable=['order_id', 'subcategory_id','quantity','total','finished'];
    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory','subcategory_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
    public function technicianOrder()
    {
        return $this->hasMany('App\TechnicianOrder');
    }
}
