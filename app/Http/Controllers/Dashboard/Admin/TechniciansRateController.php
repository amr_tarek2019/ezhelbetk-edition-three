<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\OrderRate;
use App\TechnicianRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TechniciansRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $techniciansRates=OrderRate::all();
        return view('dashboard.views.rates.technicians.index',compact('techniciansRates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $technicianRate = OrderRate::find($id);
        return view('dashboard.views.rates.technicians.show',compact('technicianRate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $technicianRate = OrderRate::find($id);
        $technicianRate->delete();
        return redirect()->back()->with('successMsg','Technician Rate Successfully Delete');
    }


}
