<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\JobDetails;
use App\Notifications;
use App\Order;
use App\OrderSubcategory;
use App\PushNotification;
use App\Setting;
use App\Subcategory;
use App\Technician;
use App\TechnicianOrder;
use App\User;
use App\UserSubscription;
use App\UserTechnicianSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('dashboard.views.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $orderDetails=OrderSubcategory::where('order_id',$id)->get();

        $data = Order::where('id',$id)->pluck('id');
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');
        $subcategories= Subcategory::whereIn('id',$subcategory_id)->get();

        return view('dashboard.views.orders.show',compact('order','orderDetails','subcategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $data = Order::where('id',$id)->pluck('id');
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');

        $technicianUsers=Technician::where('is_busy','0')->whereIn('subcategory_id',$subcategory_id)->pluck('user_id');

        $technicians=User::whereIn('id',$technicianUsers)->where('user_type','technician')->get();

        return view('dashboard.views.orders.edit',compact('order','technicians'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'status' => 'required',
            'technician' => 'required',
            'accepted'=>'required',
        ]);

        $getTechnicianId=User::where('id',$request->technician)->pluck('id');
        $technicianId=Technician::whereIn('user_id',$getTechnicianId)->pluck('id')->first();




        $order=Order::where('id',$id)->first();
        $order->status=$request->status;
        $order->accepted=$request->accepted;
        $order->save();

        if(!empty($order->subscription_id))
        {
            $userSubscriptionId=UserSubscription::where('subscription_id',$order->subscription_id)->pluck('id')->first();
            $userTechnicianSubscription=new UserTechnicianSubscription();
            $userTechnicianSubscription->technician_id=$technicianId;
            $userTechnicianSubscription->user_subscription_id=$userSubscriptionId;
            $userTechnicianSubscription->save();

        }


        $techOrder=New TechnicianOrder();
        $techOrder->order_id=$order->id;
        $techOrder->technician_id=$technicianId;
        $techOrder->order_number=$order->order_number;
        $techOrder->status= '0';
        $techOrder->accepted= $request->accepted;
        $techOrder->save();

        $jobDetails=new JobDetails();
        $jobDetails->user_id=$order->user_id;
        $jobDetails->technician_id=$techOrder->technician_id;
        $jobDetails->order_id=$order->id;
        $jobDetails->status= $order->status;
        $jobDetails->save();


        $technicianStatus = Technician::find($techOrder->technician_id);
        $technicianStatus->is_busy='1';
        $technicianStatus->save();
        $user_id=Order::where('user_id',$order->user_id)->pluck('user_id')->first();

        $token = \App\User::where('id',$user_id)->pluck('firebase_token')->toArray();

        $technicianToken=User::where('id',$getTechnicianId)->pluck('firebase_token')->toArray();



//        $token = \App\User::select('firebase_token')->where('id',$user_id)->pluck('firebase_token')->toArray();

        //PushNotification::send($token,'job successfully posted',1);
        $technId=Technician::whereIn('user_id',$getTechnicianId)->pluck('user_id')->first();
        $technicianId=User::where('id',$technId)->select('id')->first();
        $notificationTechnician=new Notifications();
        $notificationTechnician->order_id=$order->id;
        $notificationTechnician->user_id=$technicianId->id;
        $notificationTechnician->text='you have a new job posted';
        $notificationTechnician->save();

        PushNotification::send_details($token, $order->status,$order->order_number,'job successfully posted',$order->id,1);
        PushNotification::send_details($technicianToken, $order->status,$order->order_number,'you have a new job posted',$order->id,1);
        return redirect()->route('orders.index')->with('successMsg','Order Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order= Order::find($id);
        $order->delete();
        return redirect()->route('orders.index')->with('successMsg','Order Successfully Deleted');
    }

    function generatePdf($id) {
        $settings=Setting::first();
        $data = Order::findOrFail($id);
        $subcategory_id = OrderSubcategory::whereIn('order_id',$data)->pluck('subcategory_id');
        $subcategories= Subcategory::whereIn('id',$subcategory_id)->get();
        $orderSubcategories=OrderSubcategory::where('order_id',$id)->get();
        return view('dashboard.views.orders.invoice',compact('data','settings','subcategories'
        ,'orderSubcategories'));
    }

}
