<?php
namespace App\Http\Controllers\Dashboard\Admin;

use App\Notifications;
use App\PushNotification;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications=Notifications::all();
        return view('dashboard.views.notifications.index',compact('notifications'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.notifications.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users= User::where('user_status','1')->where('user_type','user')
            ->select('id','firebase_token')->get();
        foreach ($users as $user) {
            $notification = Notifications::create([
                'user_id' => $user->id,
                'text' => $request->text,
            ]);
        }
        $users= User::where('user_status','1')->where('user_type','user')->pluck('firebase_token')->first();
        PushNotification::send_details($users, null,$request->text,$notification->notification,1);
        $notification->save();
        return redirect()->route('notifications.index')->with('successMsg','Notification Successfully Sended');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification=Notifications::find($id);
        $notification->delete();
        return redirect()->route('notifications.index')->with('successMsg','Notification deleted successfully');
    }
}
